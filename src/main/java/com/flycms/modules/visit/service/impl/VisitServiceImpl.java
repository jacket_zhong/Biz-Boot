package com.flycms.modules.visit.service.impl;

import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.picture.entity.Picture;
import com.flycms.modules.visit.dao.VisitDao;
import com.flycms.modules.visit.entity.Visit;
import com.flycms.modules.visit.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 14:32 2019/11/18
 */
@Service
public class VisitServiceImpl implements VisitService {
    @Autowired
    private VisitDao visitDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 添加访问记录
     *
     * @param visit
     * @return
     */
    @Transactional
    public Object addVisit(Visit visit) {
        visit.setId(SnowFlakeUtils.nextId());
        visit.setAddTime(new Date());
        visitDao.save(visit);
        return Result.success();
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 按id删除访问记录
     *
     * @param id
     * @return
     */
    public int deleteById(Long id){
        return visitDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按id查询访问记录
     *
     * @param id
     * @return
     */
    public Visit findById(Long id){
        return visitDao.findById(id);
    }
}
