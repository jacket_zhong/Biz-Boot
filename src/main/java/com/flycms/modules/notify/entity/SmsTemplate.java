package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class SmsTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    private String  id;
    private String  apiId;
    private String  templateName;
    //模板编码
    private String  templateCode;
    private String  detail;
    private String  createTime;
    private String  status;

}
