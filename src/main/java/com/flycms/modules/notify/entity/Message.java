package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 19:58 2019/8/24
 */

@Setter
@Getter
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    //自己的userid，发信人的userid，若是为0则为系统信息
    private Long senderId;
    //给谁发信，这就是谁的userid，目标用户的userid，收信人id
    private Long recipientId;
    private String title;
    //信息内容
    private String content;
    //此信息的发送时间
    private Date addTime;
    //0:未读，1:已读 ，2已删除
    private String status;
    //是否已经被删除。0正常，1已删除，
    private String deleted;
}
