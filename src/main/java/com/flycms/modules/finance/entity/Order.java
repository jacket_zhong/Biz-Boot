package com.flycms.modules.finance.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:04 2019/8/17
 */
@Setter
@Getter
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long companyId;
    private String commodityName;
    //1,网站缴费，2模板购买，3插件购买
    private int marketType;
    private Long couponId;
    private BigDecimal couponPrice;
    private BigDecimal integralPrice;
    private Long productId;
    private String orderSn;
    private Integer orderStatus;
    private BigDecimal originalAmount;
    private BigDecimal payAmount;
    private Date endTime;
    private Date payTime;
    private Date createTime;
    private Integer deleted;
}
