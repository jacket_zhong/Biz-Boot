package com.flycms.modules.user.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.entity.Ztree;
import com.flycms.common.exception.PageAssert;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.template.entity.Template;
import com.flycms.modules.user.entity.AdminMenu;
import com.flycms.modules.user.entity.UserMenu;
import com.flycms.modules.user.service.AdminMenuService;
import com.flycms.modules.shiro.ShiroUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 18:21 2019/8/21
 */
@Controller
@RequestMapping("/admin/menu")
public class AdminMenuController extends BaseController {

    // ///////////////////////////////
    // /////  管理员菜单管理  ////////
    // ///////////////////////////////
    //管理员添加菜单
    @GetMapping("/add_admin_menu{url.suffix}")
    public String addAdminMenu(@RequestParam(value = "parentId", defaultValue = "0") String parentId,ModelMap model)
    {
        if(!StringUtils.isEmpty(parentId)){
            if(NumberUtils.isNumber(parentId)){
                AdminMenu menu=adminMenuService.findById(Long.parseLong(parentId));
                if(menu==null){
                    menu = new AdminMenu();
                    menu.setId(0l);
                    menu.setMenuName("主目录");
                }
                model.addAttribute("menu", menu);
            }
        }
        return "system/admin/user/popup/add_admin_menu";
    }

    //增加菜单信息
    @PostMapping("/add_admin_menu{url.suffix}")
    @ResponseBody
    public Object addAdminMenu(AdminMenu adminMenu)
    {
        return adminMenuService.saveMenu(adminMenu);
    }

    //批量删除角色
    @PostMapping("/del_admin_menu{url.suffix}")
    @ResponseBody
    public Object delAdminMenu(@RequestParam(value = "id", required = false) String id){
        if(StringUtils.isEmpty(id)){
            return Result.failure("角色id为空或者不存在");
        }
        PageAssert.notTrue(!NumberUtils.isNumber(id), "id参数错误");
        return adminMenuService.deleteMenuById(Long.parseLong(id));
    }

    //管理员修改菜单
    @GetMapping("/update_admin_menu{url.suffix}")
    public String updateAdminMenu(@RequestParam(value = "id", required = false) String id,Model model)
    {
        if(StringUtils.isEmpty(id) && !NumberUtils.isNumber(id)){
            return templateService.getException(model,"菜单id为空或者id参数类型错误");
        }
        AdminMenu menu=adminMenuService.findById(Long.parseLong(id));
        if(menu==null){
            return templateService.getException(model,"菜单不存在");
        }
        AdminMenu parentMenu= new AdminMenu();
        if(menu.getParentId() == 0l){
            parentMenu.setMenuName("主目录");
        }else{
            parentMenu=adminMenuService.findById(menu.getParentId());
            if(parentMenu==null){
                parentMenu = new AdminMenu();
                parentMenu.setMenuName("主目录");
            }
        }
        model.addAttribute("parentMenu", parentMenu);
        model.addAttribute("menu", menu);
        return "system/admin/user/popup/update_admin_menu";
    }

    //修改菜单信息
    @PostMapping("/update_admin_menu{url.suffix}")
    @ResponseBody
    public Object updateAdminMenu(AdminMenu adminMenu)
    {
        return adminMenuService.updateMenu(adminMenu);
    }

    //修改菜单是否显示
    @PostMapping("/visible_admin_menu{url.suffix}")
    @ResponseBody
    public Object updateAdminMenuByVisible(@RequestParam(value = "visible", required = false) Boolean visible,@RequestParam(value = "id", required = false) String id)
    {
        PageAssert.notTrue(StringUtils.isEmpty(id) && !NumberUtils.isNumber(id), "id参数错误");
        return adminMenuService.updateMenuVisible(visible,Long.parseLong(id));
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/admin_menu_tree{url.suffix}")
    public String selectMenuTree(ModelMap mmap)
    {
        return "system/admin/user/popup/admin_menu_tree";
    }

    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/admin_menu_tree_data{url.suffix}")
    @ResponseBody
    public List<Ztree> menuTreeData()
    {
        Long userId = ShiroUtils.getLoginUser().getId();
        List<Ztree> ztrees = adminMenuService.menuTreeData(userId);
        return ztrees;
    }

    @RequiresPermissions("system:menu:view")
    @GetMapping("/admin_list{url.suffix}")
    public String adminMenuList()
    {
        return "system/admin/user/list_admin_menu";
    }

    @RequiresPermissions("system:menu:list")
    @GetMapping("/admin_listData{url.suffix}")
    @ResponseBody
    public Object adminMenulistData()
    {
        Long userId = ShiroUtils.getLoginUser().getId();
        List<AdminMenu> menuList = adminMenuService.selectMenuAll(userId);
        return LayResult.success(0, "true", 0, menuList);
    }


    // ///////////////////////////////
    // /////  用户菜单管理    ////////
    // ///////////////////////////////
    //用户添加菜单
    @GetMapping("/add_user_menu{url.suffix}")
    public String addUserMenu(@RequestParam(value = "parentId", defaultValue = "0") String parentId,Model model)
    {
        if(!StringUtils.isEmpty(parentId)){
            if(NumberUtils.isNumber(parentId)){
                UserMenu menu=userMenuService.findById(Long.parseLong(parentId));
                if(menu==null){
                    menu = new UserMenu();
                    menu.setId(0L);
                    menu.setMenuName("主目录");
                }
                model.addAttribute("menu", menu);
            }
        }
        return "system/admin/user/popup/add_user_menu";
    }

    //增加菜单信息
    @PostMapping("/add_user_menu{url.suffix}")
    @ResponseBody
    public Object addUserMenu(UserMenu userMenu)
    {
        return userMenuService.insertMenu(userMenu);
    }


    //管理员修改菜单
    @GetMapping("/update_user_menu{url.suffix}")
    public String updateUserMenu(@RequestParam(value = "id", required = false) String id,Model model)
    {
        if(StringUtils.isEmpty(id) && !NumberUtils.isNumber(id)){
            return templateService.getException(model,"菜单id为空或者id参数类型错误");
        }
        UserMenu menu=userMenuService.findById(Long.parseLong(id));
        if(menu==null){
            return templateService.getException(model,"菜单不存在");
        }
        UserMenu parentMenu=new UserMenu();
        if(menu.getParentId() == 0l){
            parentMenu.setMenuName("主目录");
        }else{
            parentMenu=userMenuService.findById(menu.getParentId());
            if(parentMenu==null){
                parentMenu = new UserMenu();
                parentMenu.setMenuName("主目录");
            }
        }
        model.addAttribute("parentMenu", parentMenu);
        model.addAttribute("menu", menu);
        return "system/admin/user/popup/update_user_menu";
    }

    //修改菜单信息
    @PostMapping("/update_user_menu{url.suffix}")
    @ResponseBody
    public Object updateUserMenu(UserMenu userMenu)
    {
        return userMenuService.updateMenu(userMenu);
    }

    //修改菜单是否显示
    @PostMapping("/visible_user_menu{url.suffix}")
    @ResponseBody
    public Object updateUserMenuByVisible(@RequestParam(value = "visible", required = false) Boolean visible,@RequestParam(value = "id", required = false) String id)
    {
        PageAssert.notTrue(StringUtils.isEmpty(id) && !NumberUtils.isNumber(id), "id参数错误");
        return userMenuService.updateMenuVisible(visible,Long.parseLong(id));
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/user_menu_tree{url.suffix}")
    public String selectUserMenuTree(ModelMap mmap)
    {
        return "system/admin/user/popup/user_menu_tree";
    }

    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/user_menu_tree_data{url.suffix}")
    @ResponseBody
    public List<Ztree> userMenuTreeData()
    {
        Long userId = ShiroUtils.getLoginUser().getId();
        List<Ztree> ztrees = userMenuService.menuTreeData(userId);
        return ztrees;
    }

    @RequiresPermissions("system:menu:view")
    @GetMapping("/user_list{url.suffix}")
    public String userMenuList()
    {
        return"system/admin/user/list_user_menu";
    }

    @RequiresPermissions("system:menu:list")
    @GetMapping("/user_listData{url.suffix}")
    @ResponseBody
    public Object userMenulistData()
    {
        Long userId = ShiroUtils.getLoginUser().getId();
        List<UserMenu> menuList = userMenuService.selectMenuAll(userId);
        return LayResult.success(0, "true", 0, menuList);
    }


}
