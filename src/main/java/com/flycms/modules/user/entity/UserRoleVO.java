package com.flycms.modules.user.entity;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色VO实体类
 * 
 * @author 孙开飞
 */
@Setter
@Getter
public class UserRoleVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 角色ID */
    private String id;

    /** 角色名称 */
    private String roleName;

    /** 角色状态（0正常 1停用） */
    private Boolean status;

    /**  备注 */
    private String remark;

    /** 角色排序 */
    private String sortOrder;

}
