package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.StringUtils;
import com.flycms.modules.system.service.ConfigureService;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.template.entity.SiteTemplatePage;
import com.flycms.modules.template.entity.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 前台用户访问页面
 *
 * @author 孙开飞
 */
@Controller
public class SiteFrontController extends BaseController {
    @Autowired
    private ConfigureService configureService;

    @GetMapping(value = {"/" , "/index", "/index{url.suffix}"})
    public String index(Model model) throws IOException
    {
        String domainString=StringUtils.twoStageDomain(request.getRequestURL().toString());
        Site site = null;
        if (!org.springframework.util.StringUtils.isEmpty(domainString)) {
            site = siteService.findByDomain(domainString);
        }else{
            site = siteService.findById(Long.parseLong(systemService.findByKeyCode("default_site")));
        }
        if(site!=null){
            if(site.getTemplateId()==null){
                return templateService.getException(model,"设置模板，请进入模板市场进行购买！");
            }
            SiteTemplatePage siteTemplate=siteTemplateService.findById(site.getTemplateIndex());
            if(siteTemplate==null){
                return templateService.getException(model,"首页模板不存在或者未设置");
            }
            model.addAttribute("site",site);

            return siteTemplateService.getSiteTemplate(site.getId(),siteTemplate.getTemplateFilename());
        }else {
            return "forward:404.do";
        }
    }
}
