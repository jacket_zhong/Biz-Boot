package com.flycms.modules.site.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.system.service.SystemModuleService;
import com.flycms.modules.system.service.ConfigureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 前台分类管理
 * @email 79678111@qq.com
 * @Date: 15:26 2019/10/8
 */
@Controller
public class ColumnFrontController extends BaseController {

    @GetMapping("/{moduleType}{url.suffix}")
    public String columnShow(@PathVariable(value = "id", required = false) String id,
                             @RequestParam(value = "p", defaultValue = "1") int p,
                             ModelMap modelMap)
    {
        return "websiteTemplate/admin/index";
    }
}
