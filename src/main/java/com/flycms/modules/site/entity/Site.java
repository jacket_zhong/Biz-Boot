package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * site entitiy
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
@Setter
@Getter
public class Site implements Serializable {
	private static final long serialVersionUID = 210818469999970139L;
	private Long id;
	private Long companyId;
	private String siteName;
	private Integer showBanner;
	private Long templateId;      //启用的模板id
	private Long templateIndex;  //首页模板id，对应的是fly_site_template_page里的id，pageType是0的首页模板
	private String domain;        //网站二级域名
	private String bindDomain;	//用户自己绑定的域名
	private String logo;           //网站logo
	private String title;          //网站标题
	private String keywords;      //网站关键词
	private String description;   //网站描述
	private String copyright;     //网站版权
	private Date createTime;         //添加网站时间
	private Date expireTime;      //网站运营到期时间
	private Date updateTime;      //网站运营到期时间
	private Boolean status;      	 //站点状态，站长开关设置，1正常；0关闭
	private Integer verify;        //管理员审核状态：0未审核，1已审核，2审核未通过，3已过期
	private Integer deleted;       //网站逻辑删除

}