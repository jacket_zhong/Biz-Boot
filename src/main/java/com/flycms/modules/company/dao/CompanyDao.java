package com.flycms.modules.company.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.entity.CompanyUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface CompanyDao extends BaseDao<Company> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    public int saveCompanyUser(CompanyUser companyUser);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 按企业id删除企业与用户关联信息
     *
     * @param companyId
     * @return
     */
    public int deleteCompanyUserByCompanyId(Long companyId);

    /**
     * 按用户id删除企业与用户关联信息
     *
     * @param userId
     * @return
     */
    public int deleteCompanyUserByUserId(Long userId);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询企业全称是否已存在
     *
     * @param fullName
     * @param id
     *         需要排除的id
     * @return
     */
    public int checkCompanyByFullName(@Param("fullName") String fullName,@Param("id")Long id);

    /**
     * 查询企业简称是否已存在
     *
     * @param shortName
     *
     * @param id
     *         需要排除的id
     * @return
     */
    public int checkCompanyByShortName(@Param("shortName") String shortName,@Param("id")Long id);
    /**
     * 查询当前用户和企业是否已关联
     *
     * @param companyId
     * @param userId
     * @return
     */
    public int checkCompanyByUser(@Param("companyId") Long companyId,@Param("userId") Long userId);

    /**
     * 按用户id查询企业信息
     *
     * @param userId
     * @return
     */
    public Company findCompanyByUser(@Param("userId")Long userId);
}
