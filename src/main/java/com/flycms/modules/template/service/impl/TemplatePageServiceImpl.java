package com.flycms.modules.template.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.template.dao.TemplatePageDao;
import com.flycms.modules.template.entity.TemplatePage;
import com.flycms.modules.template.service.TemplatePageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 3:00 2019/9/12
 */
@Service
public class TemplatePageServiceImpl implements TemplatePageService {
    private static final Logger log = LoggerFactory.getLogger(TemplatePageServiceImpl.class);

    @Autowired
    private TemplatePageDao templatePageDao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 开发人员添加模板信息
     *
     * @param templatePage
     * @return
     */
    @Transactional
    public Object addDeveloperTemplatePage(TemplatePage templatePage){
        if(this.checkTemplatePageByName(templatePage.getTemplateId(),templatePage.getModuleId(),templatePage.getPageType(),templatePage.getTemplateName())){
            return Result.failure("该同类型模板页面名称已存在！");
        }
        templatePage.setId(SnowFlakeUtils.nextId());
        templatePage.setAddTime(new Date());
        templatePage.setStatus(1);//默认可使用状态
        templatePageDao.save(templatePage);
        return Result.success();
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除模板详细页面信息
     *
     * @param id
     *         模板id
     * @return
     */
    public int deleteById(Long id){
        return templatePageDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 开发人员编辑模板信息
     *
     * @param templatePage
     * @return
     */
    @Transactional
    public Object updateDeveloperTemplatePage(TemplatePage templatePage){
        if(this.checkTemplatePageByName(templatePage.getTemplateId(),templatePage.getModuleId(),templatePage.getPageType(),templatePage.getTemplateName())){
            return Result.failure("该同类型模板页面名称已存在！");
        }
        templatePage.setUpdateTime(new Date());
        templatePage.setStatus(1);//默认可使用状态
        templatePageDao.update(templatePage);
        return Result.success();
    }

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询添加模板名称是否存在
     *
     * @param templateId
     *         模板主表id
     * @param moduleId
     *         模型fly_system_module表id；对应的如新闻模块id，产品模块id
     * @param pageType
     *         模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
     * @param templateName
     *         模板名称
     * @return
     */
    public boolean checkTemplatePageByName(Long templateId, Long moduleId, Integer pageType, String templateName){
        int totalCount = templatePageDao.checkTemplatePageByName(templateId, moduleId, pageType, templateName);
        return totalCount > 0 ? true : false;
    }

    /**
     * 按id查询模板信息
     *
     * @param id
     * @return
     */
    public TemplatePage findById(Long id){
        return templatePageDao.findById(id);
    }

    /**
     * 查询当前模板拥有的所有模板页面
     *
     * @param templatePage
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectTemplatePagePager(TemplatePage templatePage, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(templatePage.getTemplateId())) {
            whereStr.append(" and template_id = #{entity.templateId}");
        }
        if (!StringUtils.isEmpty(templatePage.getTemplateName())) {
            whereStr.append(" and template_name like concat('%',#{entity.templateName},'%')");
        }
        whereStr.append(" and deleted = 0");

        Pager<TemplatePage> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        TemplatePage entity = new TemplatePage();
        entity.setTemplateName(templatePage.getTemplateName());
        entity.setTemplateId(templatePage.getTemplateId());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        List<TemplatePage> sitelsit = templatePageDao.queryList(pager);
        /*List<SiteVO> volsit = new ArrayList<SiteVO>();
        sitelsit.forEach(enttity -> {
            SiteVO siteVo = new SiteVO();
            siteVo.setId(enttity.getId());
            siteVo.setSiteName(enttity.getSiteName());
            siteVo.setDomain(enttity.getDomain());
            User user=siteDao.selectSiteUserBySiteId(enttity.getId());
            if(user!=null){
                siteVo.setUserId(user.getId());
                siteVo.setNickname(user.getNickname());
            }else{
                siteVo.setUserId(null);
                siteVo.setNickname("暂无用户");
            }
            siteVo.setAddTime(enttity.getAddTime());
            siteVo.setExpireTime(enttity.getExpireTime());
            siteVo.setStatus(enttity.getStatus());
            volsit.add(siteVo);
        });*/
        return LayResult.success(0, "true", templatePageDao.queryTotal(pager), sitelsit);
    }
}
