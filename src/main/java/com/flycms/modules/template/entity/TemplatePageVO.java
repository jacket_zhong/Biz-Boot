package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 模版页面
 * @author 孙开飞
 */
@Setter
@Getter
public class TemplatePageVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;			     //自动编号
	private String templateId;		//所属模板id
	private String moduleId;	    //模型fly_system_module表id；对应的如新闻模块id，产品模块id
	private Integer pageType;        //模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	private String templateName; //所属的模版名字
	private String remark;		//备注
	private Date addTime;
	private Integer status;
}