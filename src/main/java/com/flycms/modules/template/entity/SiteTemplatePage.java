package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @Description: 网站页面模板实体类
 *
 * 模版页面
 * @author 孙开飞
 */
@Setter
@Getter
public class SiteTemplatePage implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;			    //编号
	private Long siteId;		    //所属网站的id
	private Long templateId;		//所属模板id
	private Long moduleId;	    //模型fly_system_module表id；对应的如新闻模块id，产品模块id
	private Integer pageType;        //模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	private String templateName; //所属的模版名字
	private String templateFilename; //模版文件名称
	private String templatePage; //所属的模版页面
	private String templateCatalog; //模板目录
	private String remark;		//备注
	private Date addTime;         //添加时间
	private Date updateTime;     //最后更新时间
	private int deleted;         //逻辑删除

}