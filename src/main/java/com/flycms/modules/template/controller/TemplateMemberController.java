package com.flycms.modules.template.controller;

import com.flycms.common.exception.ApiAssert;
import com.flycms.common.pager.Pager;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.template.entity.*;
import com.flycms.modules.template.service.SiteTemplateService;
import com.flycms.modules.template.service.TemplateService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 23:53 2019/9/10
 */
@Controller
@RequestMapping("/member/template")
public class TemplateMemberController {
    @Autowired
    private SiteTemplateService siteTemplatePageService;
    @Autowired
    private SiteService siteService;
    @Autowired
    private SiteCompanyService siteCompanyService;

    @Autowired
    private TemplateService templateService;

    @GetMapping("/list{url.suffix}")
    public Object templateList(Template template,
                               @RequestParam(value = "page",defaultValue = "1") Integer page,
                               @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                               @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                               @RequestParam(defaultValue = "desc") String order,Model model) {
        Pager<Template> pager=templateService.selectTemplatePager(template, page, pageSize, sort, order);
        model.addAttribute("siteid","");
        model.addAttribute("pager",pager);
        return "system/member/site/template/list";
    }

    @GetMapping("/list_order{url.suffix}")
    public Object templateOrderList(@RequestParam(value = "siteId", required = false) String siteId,
                               @RequestParam(value = "page",defaultValue = "1") Integer page,
                               @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                               @Sort(accepts = {"add_time", "id"}) @RequestParam(defaultValue = "add_time") String sort,
                               @RequestParam(defaultValue = "desc") String order,Model model) {
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteId)), "您不是网站管理员，请勿非法操作");
        Pager<SiteTemplateOrder> pager=siteTemplatePageService.selectTemplatePager(Long.parseLong(siteId), page, pageSize, sort, order);
        model.addAttribute("siteid","");
        model.addAttribute("pager",pager);
        return "system/member/site/template/list_order";
    }

    @PostMapping("/pagelist{url.suffix}")
    @ResponseBody
    public Object pageList(@RequestParam(value = "siteId", required = false) String siteId,
                           @RequestParam(value = "channel", required = false) String channel){
        ApiAssert.notTrue(StringUtils.isEmpty(siteId) , "网站id不能为空");
        ApiAssert.notTrue(!NumberUtils.isDigits(siteId), "网站id参数错误");
        ApiAssert.notTrue(StringUtils.isEmpty(channel) , "模型id不能为空");
        ApiAssert.notTrue(!NumberUtils.isDigits(channel), "模型id参数错误");
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteId)), "您不是网站管理员，请勿非法操作");
        //理论上上面判断是否是网站管理员已判断出数据网站是否存在了，下面是肯定有数据的
        Site site=siteService.findById(Long.parseLong(siteId));
        if(site.getTemplateId()==null){
            return Result.failure("未获取到网站模板id");
        }
        List<SiteTemplatePage> pageList=siteTemplatePageService.selectTemplatePageList(Long.parseLong(siteId),site.getTemplateId(),Long.parseLong(channel),null);
        //javabean 映射工具
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();
        List<SiteTemplatePageVO> convertList= mapper.mapAsList(pageList,SiteTemplatePageVO.class);
        Map<String, Object> map = new HashMap<>();
        List<SiteTemplatePageVO> tempindex= new ArrayList<>();
        List<SiteTemplatePageVO> templist= new ArrayList<>();
        List<SiteTemplatePageVO> tempcontent= new ArrayList<>();
        List<SiteTemplatePageVO> tempalone= new ArrayList<>();
        convertList.forEach(v->{
            if(v.getPageType()==1){
                tempindex.add(v);
            }else if(v.getPageType()==2){
                templist.add(v);
            }else if(v.getPageType()==3){
                tempcontent.add(v);
            }else if(v.getPageType()==4){
                tempalone.add(v);
            }
        });
        map.put("tempindex",tempindex);
        map.put("templist",templist);
        map.put("tempcontent",tempcontent);
        map.put("tempalone",tempalone);
        return map;
    }

    @GetMapping("/buy_template{url.suffix}")
    public String add(@RequestParam(value = "id", required = false) String id,Model model)
    {
        return"system/member/order/buy_template";
    }
}
