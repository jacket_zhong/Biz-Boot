package com.flycms.modules.links.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.links.entity.Links;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @Description: 后台友情链接管理页面
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:06 2019/8/17
 */
@Controller
@RequestMapping("/admin/links/")
public class LinksAdminController extends BaseController {
    @GetMapping("/add_links{url.suffix}")
    public String addLinks(Model model) {
        return "system/admin/popup/add_links";
    }

    @PostMapping("/add_links{url.suffix}")
    @ResponseBody
    public Object addLinks(Links links){
        if(StringUtils.isEmpty(links.getLinkName())){
            return Result.failure("该网站名称不能为空");
        }
        return linksService.addLink(links);
    }


}
